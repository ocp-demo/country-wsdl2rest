#!/bin/sh
IMAGE_BUILDER=fuse7-java-openshift
IMAGE_BUILDER_TAG=1.4
# Fuse7 image not included 8080
# IMAGE_BUILDER=redhat-openjdk18-openshift
# IMAGE_BUILDER_TAG=1.5
APP_NAME=country
APP_VERSION=$(cat pom.xml | grep "<version>" | head -n 1 | awk -F">" '{print $2}' | awk -F"<" '{print $1}')
echo "######################## Create BuildConfig ########################"
oc new-build \
--binary --name=${APP_NAME} \
--image-stream=${IMAGE_BUILDER}:${IMAGE_BUILDER_TAG} \
--labels app=${APP_NAME},version=${APP_VERSION}
echo "#########################   Build JAR    ###########################"
mvn clean package
echo "###################### Start Build Container #######################"
oc start-build $APP_NAME \
--from-file=target/country-1.0.0.jar \
--follow
echo "#########################   Tag Image    ###########################"
oc tag ${APP_NAME}:latest ${APP_NAME}:${APP_VERSION}
echo "#######################   Create App   #############################"
oc new-app --image-stream=${APP_NAME}:${APP_VERSION} \
--env=APP_CONFIG=/app-config/application.properties 
echo "#######################   Pause Rollout   #########################"
oc rollout pause dc ${APP_NAME}
echo "####################   Create ConfigMap   #########################"
oc create configmap ${APP_NAME} --from-file=app-config/application.properties
echo "################   Mount ConfigMap as Volume  #####################"
oc set volume dc/${APP_NAME} --add --name=${APP_NAME}-config \
--mount-path=/app-config/application.properties \
--sub-path=application.properties \
--configmap-name=${APP_NAME}
echo "#######################   Resume Rollout   #########################"
oc rollout resume dc ${APP_NAME}
echo "#######################   Check Pods Status ########################"
watch oc get pods
echo "#####################    Create Route   ############################"
oc delete svc/${APP_NAME}
oc expose dc/${APP_NAME} --port=8080,8788,9779
oc expose svc/${APP_NAME} --port=8080
echo "URL:$(oc get route/${APP_NAME} -o jsonpath='{.spec.host}')"
